/// <reference path="../typings/index.d.ts" />
import 'moment/locale/pt-br';
import * as angular from 'angular';

import {main} from './app/state/main/main';
import {company} from './app/state/company/company';
import 'angular-ui-router';
import routesConfig from './routes';

import {mvUi} from './app/ui/index';
import {ResquestService} from './app/lib/Api/ResquestService';
import {mvHeader} from './app/components/mvHeader/mvHeader';
import {mvFooter} from './app/components/mvFooter/mvFooter';
import {mvMenu} from './app/components/mvMenu/mvMenu';
import MvMenuService from './app/components/mvMenu/mvMenuService';
import MvMenuSwitch from './app/components/mvMenu/mvMenuSwitch';
import {mvSectionCustomer} from './app/components/mvSectionCustomer/mvSectionCustomer';
import {mvSectionPartner} from './app/components/mvSectionPartner/mvSectionPartner';
import {mvSectionNews} from './app/components/mvSectionNews/mvSectionNews';

import './index.scss';

export const app: string = 'app';

angular
  .module(app, [mvUi, 'ui.router'])
  .config(routesConfig)
  .service('ResquestService', ResquestService)
  .component('app', main)
  .component('company', company)
  .component('mvHeader', mvHeader)
  .component('mvFooter', mvFooter)
  .component('mvMenu', mvMenu)
  .service('MvMenuService', MvMenuService)
  .directive('mvMenuSwitch', MvMenuSwitch)
  .component('mvSectionCustomer', mvSectionCustomer)
  .component('mvSectionNews', mvSectionNews)
  .component('mvSectionPartner', mvSectionPartner)
;

document.addEventListener('DOMContentLoaded', () => {
  angular.bootstrap(document.body, [app]);
});
