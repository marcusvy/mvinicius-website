export interface IResponse {
  success: boolean;
  collection: Array<any>;
}
