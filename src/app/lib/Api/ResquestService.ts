import {IResponse}  from './IResponse.ts';

export class ResquestService {
  private $http: ng.IHttpService;
  private $rootScope: ng.IRootScopeService;
  private collection: Array<Object>;
  private api: string;
  private success: boolean;

  /** @ngInject */
  constructor($http: ng.IHttpService, $rootScope: ng.IRootScopeService) {
    this.$http = $http;
    this.$rootScope = $rootScope;
    this.collection = [];
    this.success = false;

    this.fetch = this.fetch.bind(this);
    this.getStatus = this.getStatus.bind(this);
    this.getCollection = this.getCollection.bind(this);
    this.onFetchSuccess = this.onFetchSuccess.bind(this);
    this.onFetchFail = this.onFetchFail.bind(this);
  }

  getStatus() {
    return this.success;
  }

  getCollection(): Array<Object> {
    return this.collection;
  }

  fetch(api: string) {
    this.api = api;
    return this.$http.get(this.api)
      .then(this.onFetchSuccess, this.onFetchFail)
      .then((response) => {
        this.$rootScope.$broadcast('ResquestService.fetch');
        return response;
      });
  }

  onFetchSuccess(response: any) {
    let data: IResponse = response.data;
    if (data.success) {
      this.collection = data.collection;
      this.success = data.success;
    }
    return response;
  }

  onFetchFail(response: any) {
    this.collection = [];
    return response;
  }
}

export default ResquestService;
