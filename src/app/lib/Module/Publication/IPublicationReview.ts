import {IPhpDate} from '../../Date/IPhpDate';

export interface IPublicationReview {
  id: number;
  title: string;
  content: string;
  lead: string;
  createdat: IPhpDate;
  updatedat: IPhpDate;
  status: string,
  featured_image: string;
  thumbnail: string;
  publication: number;
}
