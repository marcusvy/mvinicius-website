import {IPhpDate} from '../../Date/IPhpDate';
import {IPublicationReview} from './IPublicationReview';

export interface IPublicationNews {
  id: number;
  author: string
  source_name: string;
  source_url: string;
  updatedat: IPhpDate,
  createdat: IPhpDate,
  publishedat: IPhpDate,
  expiredat: IPhpDate,
  revision: IPublicationReview
}
