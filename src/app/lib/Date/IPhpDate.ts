export interface IPhpDate {
  date: string;
  timezone_type: number;
  timerzone: string;
}
