/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvSectionNews} from './mvSectionNews';

describe('mvSectionNews component', () => {
  beforeEach(() => {
    angular
      .module('mvSectionNews', ['app/components/mvSectionNews/mvSectionNews.html'])
      .component('mvSectionNews', mvSectionNews);
    angular.mock.module('mvSectionNews');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvSectionNews></mvSectionNews>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
