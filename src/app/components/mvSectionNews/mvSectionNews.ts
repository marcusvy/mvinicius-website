import {ResquestService} from '../../lib/Api/ResquestService';
import {IPublicationNews} from '../../lib/Module/Publication/IPublicationNews';

class MvSectionNewsController {
  public api: string;
  public request: ResquestService;
  public publications: Array<IPublicationNews>;
  public paginator: any;
  public destaque: IPublicationNews;

  /** @ngInject */
  constructor(ResquestService: ResquestService) {
    this.request = ResquestService;
    this.publications = [];
  }

  $onInit() {
    if (this.api.length > 0) {
      this.request.fetch(this.api)
        .then(() => {
          this.publications = <any>this.request.getCollection();
          this.destaque = this.publications.shift();
        });
    }
  }
}

export const mvSectionNews = {
  templateUrl: 'app/components/mvSectionNews/mvSectionNews.html',
  controller: MvSectionNewsController,
  bindings: {
    api: '@'
  }
};
