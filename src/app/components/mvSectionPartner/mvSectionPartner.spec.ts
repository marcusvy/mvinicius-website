/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvSectionPartner} from './mvSectionPartner';

describe('mvSectionPartner component', () => {
  beforeEach(() => {
    angular
      .module('mvSectionPartner', ['app/components/mvSectionPartner/mvSectionPartner.html'])
      .component('mvSectionPartner', mvSectionPartner);
    angular.mock.module('mvSectionPartner');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvSectionPartner></mvSectionPartner>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
