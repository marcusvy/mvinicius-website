import {ResquestService} from '../../lib/Api/ResquestService';

interface IPartner {
  name: string;
  img: string;
}

class MvSectionPartnerController {
  public api: string;
  public request: ResquestService;
  public partners: Array<IPartner>;
  public paginator: any;

  /** @ngInject */
  constructor(ResquestService: ResquestService) {
    this.request = ResquestService;
    this.partners = [];
  }

  $onInit() {
    if (this.api.length > 0) {
      this.request.fetch(this.api)
        .then(() => {
          this.partners = <any>this.request.getCollection();
        });
    }
  }

}

export const mvSectionPartner = {
  templateUrl: 'app/components/mvSectionPartner/mvSectionPartner.html',
  controller: MvSectionPartnerController,
  bindings: {
    api: '@'
  }
};
