import {IResponse}  from './../../lib/Api/IResponse.ts';

export class MvMenuService {
  private $http: ng.IHttpService;
  private $rootScope: ng.IRootScopeService;
  private collection: Array<Object>;
  private api: string;
  private failed: boolean;
  private opened: boolean;

  /** @ngInject */
  constructor($http: ng.IHttpService, $rootScope: ng.IRootScopeService) {
    this.$http = $http;
    this.$rootScope = $rootScope;
    this.collection = [];
    this.failed = true;
    this.opened = false;

    this.fetch = this.fetch.bind(this);
    this.onFetchFail = this.onFetchFail.bind(this);
    this.onFetchSuccess = this.onFetchSuccess.bind(this);
    this.isOpen = this.isOpen.bind(this);
    this.toggle = this.toggle.bind(this);
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
  }

  get STATE_ON_OPEN(): string {
    return 'MvMenuService.open';
  }

  get STATE_ON_CLOSE(): string {
    return 'MvMenuService.close';
  }

  isOpen() {
    return this.opened;
  }

  toggle() {
    return (this.opened) ? this.close() : this.open();
  }

  open() {
    this.opened = true;
    this.$rootScope.$broadcast('MvMenuService.open');
  }

  close() {
    this.opened = false;
    this.$rootScope.$broadcast('MvMenuService.close');
  }

  getFailed() {
    return this.failed;
  }

  getCollection(): Array<Object> {
    return this.collection;
  }

  fetch(api: string) {
    this.api = api;
    this.$rootScope.$broadcast('MvMenuService.fetch');
    return this.$http.get(this.api)
      .then(this.onFetchSuccess, this.onFetchFail);
  }

  onFetchSuccess(response: any) {
    let data: IResponse = response.data;
    if (data.success) {
      this.collection = data.collection;
      this.failed = true;
    }
    return response;
  }

  onFetchFail(response: any) {
    this.collection = [];
    return response;
  }
}

export default MvMenuService;


