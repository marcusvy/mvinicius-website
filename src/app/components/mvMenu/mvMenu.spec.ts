/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvMenu} from './mvMenu';

describe('mvMenu component', () => {
  beforeEach(() => {
    angular
      .module('mvMenu', ['app/components/mvMenu/mvMenu.html'])
      .component('mvMenu', mvMenu);
    angular.mock.module('mvMenu');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvMenu></mvMenu>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
