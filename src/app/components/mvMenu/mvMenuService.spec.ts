/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import MvMenuService from './mvMenuService';

describe('MvMenuService service', () => {
  beforeEach(() => {
    angular
      .module('MvMenuService', [])
      .service('MvMenuService', MvMenuService);
    angular.mock.module('MvMenuService');
  });

  it('should', angular.mock.inject((MvMenuService: MvMenuService) => {
    expect(MvMenuService.getData()).toEqual(3);
  }));
});
