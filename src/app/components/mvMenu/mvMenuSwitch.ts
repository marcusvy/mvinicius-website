import {MvMenuService} from './mvMenuService';

class MvMenuSwitchController {

  private $scope: ng.IScope;
  private MenuService: MvMenuService;

  /** @ngInject */
  constructor(MvMenuService: MvMenuService, $scope: ng.IScope) {
    this.MenuService = MvMenuService;
    this.$scope = $scope;

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.MenuService.toggle();
    this.$scope.$apply();
  }
}

function mvMenuSwitch() {
  return {
    restrict: 'A',
    scope: {},
    controller: MvMenuSwitchController,
    controllerAs: '$ctrl',
    bindToController: true,
    link: (scope, element, attrs, ctrl) => {
      element[0].addEventListener('click', ctrl.toggle);
    }
  };
}

export default mvMenuSwitch;
