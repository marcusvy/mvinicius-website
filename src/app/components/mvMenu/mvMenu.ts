// import {IResponse} from './../../lib/Api/IResponse';
import {MvMenuService} from './mvMenuService';

export class MvMenuController {
  public menus: Array<Object>;
  private $element: JQuery;
  private api: string;
  private opened: boolean;
  private failed: boolean;
  private MenuService: MvMenuService;

  /** @ngInject */
  constructor($element: JQuery, $scope: ng.IScope, MvMenuService: MvMenuService, $rootScope: ng.IRootScopeService) {
    this.$element = $element;
    this.failed = true;
    this.opened = MvMenuService.isOpen();
    this.MenuService = MvMenuService;
    this.menus = [];

    if (this.api.length > 0) {
      this.onFetch();
    }

    $scope.$on(MvMenuService.STATE_ON_OPEN, () => {
      this.opened = MvMenuService.isOpen();
    });
    $scope.$on(MvMenuService.STATE_ON_CLOSE, () => {
      this.opened = MvMenuService.isOpen();
    });

    $rootScope.$on('$stateChangeSuccess', ($event)=>{
      this.MenuService.close();
    });
  }

  isOpen() {
    return this.opened;
  }

  onFetch() {
    this.MenuService.fetch(this.api)
      .then(() => {
          this.menus = this.MenuService.getCollection();
          this.failed = this.MenuService.getFailed();
        }, () => {
          this.failed = this.MenuService.getFailed();
        }
      );
  }
}

export const mvMenu = {
  templateUrl: 'app/components/mvMenu/mvMenu.html',
  controller: MvMenuController,
  bindings: {
    api: '@'
  }
};

