class MvHeaderController {
  public fixed: string;
  public element: HTMLElement;

  /** @ngInject */
  constructor($element: Element) {
    this.element = $element[0];

    this._onScroll = this._onScroll.bind(this);
    this.$onInit = this.$onInit.bind(this);
    this.isFixed = this.isFixed.bind(this);
    this.addEventListeners();
  }

  $onInit() {
    if (this.isFixed()) {
      document.body.classList.add('mv-header--fixed');
    }
  }

  isFixed() {
    return (this.fixed !== undefined);
  }

  addEventListeners() {
    if (!this.isFixed()) {
      document.addEventListener('scroll', (event: Event) => this._onScroll(event));
    }
  }

  _onScroll(event: Event) {
    let elementRoot = document.body,
      scrollPositionY = window.scrollY - this.element.offsetHeight;

    if (scrollPositionY > 0) {
      elementRoot.classList.add('mv-header--fixed');
    } else {
      elementRoot.classList.remove('mv-header--fixed');
    }
  }

}

export const mvHeader = {
  templateUrl: 'app/components/mvHeader/mvHeader.html',
  controller: MvHeaderController,
  bindings: {
    fixed: '@'
  }
};

