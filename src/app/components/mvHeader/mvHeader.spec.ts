/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvHeader} from './mvHeader';

describe('mvHeader component', () => {
  beforeEach(() => {
    angular
      .module('mvHeader', ['app/components/mvHeader/mvHeader.html'])
      .component('mvHeader', mvHeader);
    angular.mock.module('mvHeader');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvHeader></mvHeader>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
