/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvSectionCustomer} from './mvSectionCustomer';

describe('mvSectionCustomer component', () => {
  beforeEach(() => {
    angular
      .module('mvSectionCustomer', ['app/components/mvSectionCustomer/mvSectionCustomer.html'])
      .component('mvSectionCustomer', mvSectionCustomer);
    angular.mock.module('mvSectionCustomer');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvSectionCustomer></mvSectionCustomer>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
