import {ResquestService} from '../../lib/Api/ResquestService';

interface ICustomer {
  name: string;
  img: string;
}

class MvSectionCustomerController {
  public api: string;
  public request: ResquestService;
  public customers: Array<ICustomer>;
  public paginator: any;

  /** @ngInject */
  constructor(ResquestService: ResquestService) {
    this.request = ResquestService;
    this.$onInit = this.$onInit.bind(this);
    this.fetch = this.fetch.bind(this);
  }

  fetch() {
    if (this.api.length > 0) {
      this.request.fetch(this.api)
        .then((response) => {
          // this.customers = <any>this.request.getCollection();
          this.customers = response.data.collection;
        });
    }
  }

  $onInit() {
    this.fetch();
  }
}

export const mvSectionCustomer = {
  templateUrl: 'app/components/mvSectionCustomer/mvSectionCustomer.html',
  controller: MvSectionCustomerController,
  bindings: {
    api: '@'
  }
};
