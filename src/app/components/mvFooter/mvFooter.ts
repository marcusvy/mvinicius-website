class MvFooterController {
  public text: string;

  constructor() {
    this.text = 'Footer';
  }
}

export const mvFooter = {
  templateUrl: 'app/components/mvFooter/mvFooter.html',
  controller: MvFooterController
};
