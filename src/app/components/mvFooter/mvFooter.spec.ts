/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvFooter} from './mvFooter';

describe('mvFooter component', () => {
  beforeEach(() => {
    angular
      .module('mvFooter', ['app/components/mvFooter/mvFooter.html'])
      .component('mvFooter', mvFooter);
    angular.mock.module('mvFooter');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvFooter></mvFooter>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
