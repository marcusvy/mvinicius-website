/// <reference path="../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {company} from './company';

describe('company component', () => {
  beforeEach(() => {
    angular
      .module('company', ['app/company.html'])
      .component('company', company);
    angular.mock.module('company');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<company></company>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
