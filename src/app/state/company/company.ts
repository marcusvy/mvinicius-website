class CompanyController {
  public text: string;

  constructor() {
    this.text = 'My brand new component!';
  }
}

export const company = {
  templateUrl: 'app/state/company/company.html',
  controller: CompanyController
};

