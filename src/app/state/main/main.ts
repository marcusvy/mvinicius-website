class MainController {
  public text: string;

  constructor() {
    this.text = 'My brand new component!';
  }
}

export const main = {
  templateUrl: 'app/state/main/main.html',
  controller: MainController
};

