/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvServiceIcon} from './mvServiceIcon';

describe('mvServiceIcon component', () => {
  beforeEach(() => {
    angular
      .module('mvServiceIcon', ['app/components/mvServiceIcon/mvServiceIcon.html'])
      .component('mvServiceIcon', mvServiceIcon);
    angular.mock.module('mvServiceIcon');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvServiceIcon></mvServiceIcon>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
