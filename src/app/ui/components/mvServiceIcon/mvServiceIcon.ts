class MvServiceIconController {
  private name: string;

  constructor() {
    this.name = '';
  }
}
export const mvServiceIcon = {
  templateUrl: 'app/ui/components/mvServiceIcon/mvServiceIcon.html',
  controller: MvServiceIconController,
  bindings: {
    name: '@?'
  }
};
