class MvSectionController {
  private title: string;

  /** @ngInject */
  constructor() {
    if (this.title === undefined) {
      this.title = '';
    }
  }
}

export const mvSection = {
  templateUrl: 'app/ui/components/mvSection/mvSection.html',
  controller: MvSectionController,
  bindings: {
    title: '@title'
  },
  transclude: {
    'content': '?mvSectionContent',
    'list': '?mvSectionList',
    'paginator': '?mvSectionPaginator'
  }
};




