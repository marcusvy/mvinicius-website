/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvSection} from './mvSection';

describe('mvSection component', () => {
  beforeEach(() => {
    angular
      .module('mvSection', ['app/components/mvSection/mvSection.html'])
      .component('mvSection', mvSection);
    angular.mock.module('mvSection');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvSection></mvSection>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
