# mv-section

**Atributos**
 
- title: Título da seção 

**Seções disponíveis**

* content: espaço para texto ou descrição lateral
* paginator: espaço para paginação de dados
* list: espaço para listagem de dados

## Utilizando elemento mv-grid
```html
<mv-section title="Um título aqui">
  <mv-section-paginator>
    ...
  </mv-section-paginator>
  <mv-section-content>
    ...
  </mv-section-content>
  <mv-section-list>
    ...
  </mv-section-list>
</mv-section>
```
