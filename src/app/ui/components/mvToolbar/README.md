# mv-toolbar
Componente de barra

## Elementos
```html
<mv-toolbar>
  <div class="mv-toolbar__row">
    <mv-sidebar-switch data-target="mainSidebar" accesskey="2">
      <mv-icon>apps</mv-icon>
    </mv-sidebar-switch>
    <h1>Titulo</h1>
    <span class="mv-layout--flex"></span>
    <mv-sidebar-switch data-target="toolSidebar">
      <mv-icon>star</mv-icon>
    </mv-sidebar-switch>
    <mv-sidebar-switch data-target="toolSidebar" accesskey="3">
      <mv-icon>images</mv-icon>
    </mv-sidebar-switch>
  </div>
</mv-toolbar>
```
