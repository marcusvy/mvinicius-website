# mv-layout

## Classes
```html
<div class="mv-row">
  <div class="mv-col mv-col--4">
    <p>Estudo</p>
  </div>
  <div class="mv-col mv-col--8">
    <h1>Estudo</h1>
    <h2>Legal</h2>'
    <p>Parágrafo</p>
  </div>
</div>
```

## Elementos
```html
<mv-row>  
  <mv-col class="mv-col--4">
    <p>Estudo</p>
  </mv-col>
  <mv-col class="mv-col--8">
    <h1>Estudo</h1>
    <h2>Legal</h2>'
    <p>Parágrafo</p>
  </mv-col>
</mv-row>
```

## Delimitação

Classes para delimitação de largura máxima. Ideal utilizar com **mv-layout--container**; 


* .mv-layout--limit-xs: max-width: **480px**;
* .mv-layout--limit-sm: max-width: **768px**;
* .mv-layout--limit-md: max-width: **992px**;
* .mv-layout--limit-lg: max-width: **1200px**;

```html
<div class="mv-device--limit-xs"></div>
<div class="mv-device--limit-sm"></div>
<div class="mv-device--limit-md"></div>
<div class="mv-device--limit-lg"></div>
```
