import * as moment from 'moment';

export function mvMoment() {
  return (item: string, format: string) => {
    if (format === undefined) {
      format = 'DD/MM/YYYY hh:mm';
    }
    return moment(item).format(format);
  };
}

export default mvMoment;

