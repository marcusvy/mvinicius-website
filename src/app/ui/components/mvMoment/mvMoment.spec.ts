/// <reference path="../../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvMoment} from './mvMoment';

describe('mvMoment filter', () => {
  beforeEach(() => {
    angular
      .module('mvMoment', [])
      .component('mvMoment', mvMoment);
    angular.mock.module('mvMoment');
  });

  it('should...', angular.mock.inject(mvMomentFilter => {
    expect(mvMomentFilter('Hello')).toBe(0);
  }));
});
