/// <reference path="../../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvControl} from './mvControl';

describe('mvControl component', () => {
  beforeEach(() => {
    angular
      .module('mvControl', ['app/ui/components/mvControl/mvControl.html'])
      .component('mvControl', mvControl);
    angular.mock.module('mvControl');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvControl></mvControl>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
