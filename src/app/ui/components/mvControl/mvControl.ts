class MvControlController {
  public text: string;

  constructor() {
    this.text = 'My brand new component!';
  }
}

export const mvControl = {
  templateUrl: 'app/ui/components/mvControl/mvControl.html',
  controller: MvControlController
};

