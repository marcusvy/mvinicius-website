#MV Button
--------------------

Elemento Botão

##Dependências
Não necessita de pacotes

##Uso

```html
<button class="mv-button">Default</button>
```

## Cores
 
```html
<button class="mv-button mv-button--primary">primary</button>
<button class="mv-button mv-button--danger">danger</button>
<button class="mv-button mv-button--warning">warning</button>
<button class="mv-button mv-button--success">success</button>
<button class="mv-button mv-button--light">light</button>
<button class="mv-button mv-button--dark">dark</button>
<button class="mv-button mv-button--accent">accent</button>
```

##Tipos

###Fab
```html
<button class="mv-button mv-button--fab">
  <i class="mv-icon">apps</i>
</button>
```

###Action (ação)
```html
<button class="mv-button mv-button--action">
  <i class="mv-icon">close</i>
</button>
```
