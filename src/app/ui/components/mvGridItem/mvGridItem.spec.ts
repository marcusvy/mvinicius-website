/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvGridItem} from './mvGridItem';

describe('mvGridItem component', () => {
  beforeEach(() => {
    angular
      .module('mvGridItem', ['app/components/mvGridItem/mvGridItem.html'])
      .component('mvGridItem', mvGridItem);
    angular.mock.module('mvGridItem');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvGridItem></mvGridItem>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
