import {MvGridController} from './../mvGrid/mvGrid';

class MvGridItemController {
  private $element: HTMLElement;
  private icon: string;
  private title: string;
  private img: string;
  private link: string;
  private target: string;
  private date: string;
  private mvGrid: MvGridController;

  /** ngInject */
  constructor($element: HTMLElement) {
    this.target = '_self';

    if (this.title === undefined) {
      this.title = '';
    }
    if (this.icon === undefined) {
      this.icon = 'circle';
    }
    this.$element = $element;
  }

  hasImg() {
    return (this.img !== undefined);
  }

  hasLink() {
    return (this.link !== undefined);
  }

  hasDate() {
    return (this.date !== undefined);
  }

  enableCssClass(device: string, size: number) {
    return `mv-col--${device}-${size.toString()}`;
  }

  injectLayoutClasses(mvGrid: MvGridController) {

    let el = this.$element[0].parentNode;
    el.classList.add('mv-col');
    if (mvGrid === null) {
      return;
    }
    if (mvGrid.xs !== undefined) {
      el.classList.add(this.enableCssClass('xs', mvGrid.xs));
    }
    if (mvGrid.sm !== undefined) {
      el.classList.add(this.enableCssClass('sm', mvGrid.sm));
    }
    if (mvGrid.md !== undefined) {
      el.classList.add(this.enableCssClass('md', mvGrid.md));
    }
    if (mvGrid.lg !== undefined) {
      el.classList.add(this.enableCssClass('lg', mvGrid.lg));
    }

  }

  $onInit() {
    this.injectLayoutClasses(this.mvGrid);
  }

}

export const mvGridItem = {
  require: {
    mvGrid: '?^^'
  },
  templateUrl: 'app/ui/components/mvGridItem/mvGridItem.html',
  controller: MvGridItemController,
  transclude: true,
  bindings: {
    icon: '@',
    img: '@',
    title: '@',
    link: '@',
    target: '@',
    date: '@'
  }
};

