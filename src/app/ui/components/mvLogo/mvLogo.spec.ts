/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvLogo} from './mvLogo';

describe('mvLogo component', () => {
  beforeEach(() => {
    angular
      .module('mvLogo', ['app/components/mvLogo/mvLogo.html'])
      .component('mvLogo', mvLogo);
    angular.mock.module('mvLogo');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvLogo></mvLogo>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
