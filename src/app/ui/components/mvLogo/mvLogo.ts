class MvLogoController {

  public text: string;
  public model: string;

  /** @ngInject */
  constructor($element: JQuery, $attrs: any) {
    this.model = ('model' in $attrs) ? $attrs.model : 'default';
    this.defineMode($element);
  }

  defineMode($element: JQuery): void {
    $element[0].classList.add(`mv-logo--${this.model}`);
  }

}

export const mvLogo = {
  templateUrl: 'app/ui/components/mvLogo/mvLogo.html',
  bindings: '?<',
  controller: MvLogoController
};
