# mv-logo
Componente de logomarca MVinicius Consultoria

## Elemento
```html
<mv-logo><mv-logo>
<mv-logo model="compact"><mv-logo>
<mv-logo model="horizontal"><mv-logo>
```

## Atributos

* model [string] ("default"):
  Modelo da logomarca. Pode ser "default" ou "horizontal"
