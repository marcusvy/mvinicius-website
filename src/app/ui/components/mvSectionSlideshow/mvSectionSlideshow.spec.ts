/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvSectionSlideshow} from './mvSectionSlideshow';

describe('mvSectionSlideshow component', () => {
  beforeEach(() => {
    angular
      .module('mvSectionSlideshow', ['app/components/mvSectionSlideshow/mvSectionSlideshow.html'])
      .component('mvSectionSlideshow', mvSectionSlideshow);
    angular.mock.module('mvSectionSlideshow');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvSectionSlideshow></mvSectionSlideshow>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
