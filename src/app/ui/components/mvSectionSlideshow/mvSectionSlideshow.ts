import {IResponse} from './../../../lib/Api/IResponse';
import IIntervalService = angular.IIntervalService;

interface ISlideshowJson {
  id: number;
  local: string;
  slides: Array<Object>;
}

class MvSectionSlideshowController {

  public failed: boolean;
  public duration: number;
  public page: number;
  public fullscreen: boolean;
  private $interval: IIntervalService;
  private $http: ng.IHttpService;
  private $scope: ng.IScope;
  private $element: JQuery;
  private api: string;
  private slideshow: ISlideshowJson;
  private slides: Array<Object>;
  private timer: any;
  private timerRequest: any;
  private timerStart: any;
  private timerProgress: number;
  private progressElement: HTMLElement;
  private fullscreenElement: HTMLElement;

  /** @ngInject */
  constructor($http: ng.IHttpService, $interval: IIntervalService, $element: JQuery, $scope: ng.IScope) {
    this.$http = $http;
    this.$scope = $scope;
    this.$interval = $interval;
    this.$element = $element;
    // this.api = '';
    this.page = 1;
    this.failed = true;
    this.duration = 10000; // in miliseconds : 10s
    this.timerStart = 0;

    this.$onInit = this.$onInit.bind(this);
    this.goToPage = this.goToPage.bind(this);
    this.nextPage = this.nextPage.bind(this);
    this.startClock = this.startClock.bind(this);
    this.isCurrentPage = this.isCurrentPage.bind(this);
    this.startClock = this.startClock.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
    this.isFullscreen = this.isFullscreen.bind(this);
    this.onUpdateFullscreen = this.onUpdateFullscreen.bind(this);
    this.onFetch = this.onFetch.bind(this);
  }

  $onInit() {
    this.fullscreenElement = <HTMLElement>this.$element[0].children[0];
    this.progressElement = <HTMLElement>this.$element[0].querySelectorAll('.mv-section-slideshow__progress')[0];

    if (this.api.length > 0) {
      this.onFetch();
    }

    if (this.isFullscreen()) {
      this.$element[0].classList.add('mv-section-slideshow--fullscreen');
    }
  }

  getSlides() {
    return this.slides;
  }

  isCurrentPage(index: number) {
    return this.page === (index);
  }

  goToPage(page: number) {
    this.page = page;
    cancelAnimationFrame(this.timerProgress);
    cancelAnimationFrame(this.timer);
    this.startClock();
    this.$scope.$apply();
    return this.page;
  }

  nextPage() {
    let totalSlides = this.slides.length;
    let page = this.page;
    if (this.page < totalSlides) {
      page++;
    } else {
      page = 1;
    }
    this.goToPage(page);
  }

  startClock() {
    this.progressElement.style.width = `0%`;
    this.timer = requestAnimationFrame((time) => {
      this.timerStart = time || new Date().getTime();
      this.onUpdate(time);
      this.onUpdateFullscreen();
    });
  }

  onUpdate(time: number) {
    let runtime = time - this.timerStart;

    this.timerProgress = Math.abs(runtime / this.duration) * 100;
    if (runtime < this.duration) {
      this.timerRequest = requestAnimationFrame((t) => this.onUpdate(t));
      this.progressElement.style.width = `${this.timerProgress}%`;
    } else {
      this.nextPage();
      this.onUpdateFullscreen();
      cancelAnimationFrame(this.timerRequest);
    }
  }

  isFullscreen() {
    return (this.fullscreen !== undefined);
  }

  onUpdateFullscreen() {
    if (this.isFullscreen()) {
      let slide: any = this.slides[this.page - 1];
      this.fullscreenElement.style.backgroundImage = `url("${slide.midia.uri}")`;
    }
  }

  onFetch() {
    this.$http.get(this.api)
      .then((response: any) => {
          let data: IResponse = response.data;

          if (data.success) {
            this.slideshow = data.collection[0];
            this.slides = this.slideshow.slides;
            this.failed = false;
            this.startClock();
          }
          return response;
        },
        (response: any) => {
          this.slides = [];
          this.failed = true;
          return response;
        });
  }

  $onDestroy() {
    cancelAnimationFrame(this.timerProgress);
    cancelAnimationFrame(this.timer);
  }
}

export const mvSectionSlideshow = {
  templateUrl: 'app/ui/components/mvSectionSlideshow/mvSectionSlideshow.html',
  controller: MvSectionSlideshowController,
  bindings: {
    duration: '@?',
    api: '@',
    fullscreen: '@'
  }
};
