$.trumbowyg.svgPath = '/assets/fonts/icons.svg';

export class MvControlEditorElement extends HTMLElement {
  createdCallback() {
    this.defaultButtons = [
      // ['viewHTML'],
      ['formatting'],
      'btnGrp-semantic',
      ['superscript', 'subscript'],
      ['link'],
      'btnGrp-justify',
      'btnGrp-lists',
      ['horizontalRule'],
      ['removeformat'],
      ['fullscreen']
    ];

    this.defaultConfig = {
      btns: this.defaultButtons,
      lang: 'pt',
      removeformatPasted: true
    };

    this.component = $(this);
    this.textarea = $(this).find('textarea');
    this.label = $(this).find('.mv-control__label');
    this.value = $(this).find('.mv-control__value');
    this.switch = $(this).find('.mv-control__switch');
    this.editor = $(this).find('.trumbowyg-editor');
    this.content = $(this).find('.mv-control__preview');

    this.init = this.init.bind(this);
    this.destroy = this.destroy.bind(this);
    this.focus = this.focus.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onToggle = this.onToggle.bind(this);
    this.updateValue = this.updateValue.bind(this);
    this.updateRange = this.updateRange.bind(this);

    this.addEventListeners();
    this.updateValue();
    this.updateRange();
    if (this.component.hasClass('.js-mv-control--enabled')) {
      this.init();
    }
  }

  isEnabled() {
    return this.textarea.hasClass('trumbowyg-textarea');
  }

  init() {
    if (!this.isEnabled()) {
      return this.textarea
        .trumbowyg(this.defaultConfig)
        .on('tbwchange', this.onChange);
    }
    return this.textarea;
  };

  destroy() {
    this.textarea.trumbowyg('destroy');
  }

  focus() {
    this.editor.focus();
  }

  updateRange() {
    let value = this.textarea.val().length;
    this.value.text(`${value} caracteres`);
  }

  updateValue() {
    let value = this.textarea.val();
    this.content.html(value);
  }

  onChange() {
    this.updateRange();
    this.updateValue();
  }

  onToggle() {
    this.component.toggleClass('mv-control--open');
    if (this.isEnabled()) {
      this.destroy();
    } else {
      this.init();
      this.focus();
    }
    this.updateValue();
  }

  addEventListeners() {
    this.switch.on('click', this.onToggle);
    this.textarea.on('keypress', this.updateRange);
  }
}
