export class MvControlElement extends HTMLElement {

  constructor(){
    return super();
  }

  createdCallback() {
    this.label = ('label' in this.dataset) ? this.dataset.label : false;
    this.icon = ('icon' in this.dataset) ? this.dataset.icon : false;
    this.messages = ('messages' in this.dataset) ? JSON.parse(this.dataset.messages) : false;

    this.init();
  }

  init() {
    this.classList.add('mv-control');
    if (this.icon) {
      this.classList.add('mv-control--icon');
    }
  }
}
