import Mustache from 'mustache';
import template from '../mv-control.html';

export class MvControlSelectElement extends HTMLElement {

  createdCallback() {
    this.label = ('label' in this.dataset) ? this.dataset.label : false;
    this.icon = ('icon' in this.dataset) ? this.dataset.icon : false;
    this.messages = ('messages' in this.dataset) ? JSON.parse(this.dataset.messages) : false;

    this.init();
    this.addEventListeners();
    this.render();
  }

  init() {
    this.classList.add('mv-control');
    this.classList.add('mv-control--select');
    if (this.icon) {
      this.classList.add('mv-control--icon');
    }
  }

  render() {
    const view = {
      icon: this.icon,
      label: this.label,
      message: this.messages,
      messages: ()=> this._messagesRender,
      control: this.innerHTML
    };
    this.innerHTML = Mustache.render(template, view);
  }

  _messagesRender(partial, render) {
    if (this.message) {
      this.message = Object.keys(this.message)
        .map(k=>this.message[k]);
    }
    return (Object.keys(this.message).length > 0) ? render(partial) : '';
  }

  addEventListeners() {

  }
}
