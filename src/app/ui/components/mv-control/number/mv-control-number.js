import Modernizr from 'modernizr';
import Mustache from 'mustache';
import template from './template.html';

export class MvControlNumberElement extends HTMLElement {

  createdCallback() {
    this.label = ('label' in this.dataset) ? this.dataset.label : false;
    this.icon = ('icon' in this.dataset) ? this.dataset.icon : false;
    this.messages = ('messages' in this.dataset) ? JSON.parse(this.dataset.messages) : false;

    this.up = this.up.bind(this);
    this.down = this.down.bind(this);

    this.render();

    this.input = $(this).find('input');
    this.buttonUp = $(this).find('.mv-control__button-up');
    this.buttonDown = $(this).find('.mv-control__button-down');
    this.value = this.input.val();

    this.init();
    this.addEventListeners();
  }

  init() {
    this.classList.add('mv-control');
    this.classList.add('mv-control--number');
    if (this.icon) {
      this.classList.add('mv-control--icon');
    }
  }

  checkCompatibility() {

  }

  render() {
    const view = {
      icon: this.icon,
      label: this.label,
      message: this.messages,
      messages: ()=> this._messagesRender,
      control: this.innerHTML
    };
    this.innerHTML = Mustache.render(template, view);
  }

  _messagesRender(partial, render) {
    if (this.message) {
      this.message = Object.keys(this.message)
        .map(k=>this.message[k]);
    }
    return (Object.keys(this.message).length > 0) ? render(partial) : '';
  }

  addEventListeners() {
    this.buttonUp.on('click', this.up);
    this.buttonDown.on('click', this.down);
  }

  up() {
    this.value++;
    this.input.val(this.value);
  }

  down(){
    this.value--;
    this.input.val(this.value);
  }
}
