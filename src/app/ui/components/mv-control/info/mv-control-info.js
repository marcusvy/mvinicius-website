import Mustache from 'mustache';
import template from './template.html';

export class MvControlInfoElement extends HTMLElement {
  createdCallback() {
    this.label = ('label' in this.dataset) ? this.dataset.label : false;
    this.icon = ('icon' in this.dataset) ? this.dataset.icon : false;

    this.init();
    this.addEventListeners();
    this.render();
  }

  init() {
    this.classList.add('mv-control');
    if(this.icon){
      this.classList.add('mv-control--icon');
    }
  }

  render(){
    const view = {
      icon: this.icon,
      label: this.label,
      value: this.innerHTML
    };
    this.innerHTML = Mustache.render(template, view);
  }

  addEventListeners() {

  }
}
