# mv-grid

## Método com mv-layout
```html
<div class="mv-layout--grid">
  <div class="mv-col mv-col--xs-12 mv-col--sm-6 mv-col--md-4">
    <mv-grid-item img="/local/imgagem.jpg">
      ...
    </mv-grid-item>
  </div>
</div>
```

## Utilizando elemento mv-grid
```html
<mv-grid xs="12" sm="6" md="4" lg="3">
  <mg-grid-col ng-repeat="item in [1,2,3,4,5,6,7,8,9,10]">
    <mv-grid-item img="/local/media/image/2016/07/desert_thumbail_medium_320x150.jpg" link="#/dot">
     ...
    </mv-grid-item>
  </mg-grid-col>
</mv-grid>
```

## Delimitação

Classes 
