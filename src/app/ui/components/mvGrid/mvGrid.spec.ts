/// <reference path="../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvGrid} from './mvGrid';

describe('mvGrid component', () => {
  beforeEach(() => {
    angular
      .module('mvGrid', ['app/components/mvGrid/mvGrid.html'])
      .component('mvGrid', mvGrid);
    angular.mock.module('mvGrid');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvGrid></mvGrid>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
