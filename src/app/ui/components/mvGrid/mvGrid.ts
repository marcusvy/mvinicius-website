export class MvGridController {
  public xs: number;
  public sm: number;
  public md: number;
  public lg: number;
}

export const mvGrid = {
  templateUrl: 'app/ui/components/mvGrid/mvGrid.html',
  controller: MvGridController,
  transclude: true,
  bindings: {
    xs: '@',
    sm: '@',
    md: '@',
    lg: '@'
  }
};

