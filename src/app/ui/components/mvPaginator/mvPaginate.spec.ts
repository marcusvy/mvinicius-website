/// <reference path="../../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import mvPaginate from './mvPaginate';

describe('mvPaginate filter', () => {
  beforeEach(() => {
    angular
      .module('mvPaginate', [])
      .component('mvPaginate', mvPaginate);
    angular.mock.module('mvPaginate');
  });

  it('should...', angular.mock.inject(mvPaginateFilter => {
    expect(mvPaginateFilter('Hello')).toBe(0);
  }));
});
