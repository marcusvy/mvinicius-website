export interface IFilterMvPaginate extends ng.IFilterService {
  <T>(name: 'mvPaginate'): (input: T[], start: number|string) => T[];
}

export function mvPaginate() {
  return (input: any, start: string): IFilterMvPaginate => {
    return input.slice(parseInt(start, 10));
  };
}

export default mvPaginate;
