/// <reference path="../../../../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {mvPaginator} from './mvPaginator';

describe('mvPaginator component', () => {
  beforeEach(() => {
    angular
      .module('mvPaginator', ['app/ui/components/mvPaginator/mvPaginator.html'])
      .component('mvPaginator', mvPaginator);
    angular.mock.module('mvPaginator');
  });

  it('should...', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<mvPaginator></mvPaginator>')($rootScope);
    $rootScope.$digest();
    expect(element).not.toBeNull();
  }));
});
