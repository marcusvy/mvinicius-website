# mv-paginator

Adiciona um controlador de paginação

**Atributos**
 
- limit (number): Limite de dados por página. Ex.: 6;
- order-by (string): Ordena pelo campo especificado. Ex.: "name";
- collection (array): Array de objetos a serem filtrados. Ex.: $ctrl.customers;
- result (array): Ligação (two way) com objeto que será o resultado da paginação. Ex.: $ctrl.paginator


Implementação
-------------------------------------
1. Adicionar objeto de resultado no controller:

ES6
```js
class CustomerController {
  constructor(){
    this.customers = [];
    this.paginator = [];
  }
}
```
Typescript
```js
class CustomerController {
  public customers: Array<Customer>;
  public paginator: any;
  
  constructor(){
    this.customers = [];
    this.paginator = [];
  }
}
```

2. Adicionar o elemento de paginação e especificar o objeto de coleção e resultado:

```html
<mv-paginator limit="6" order-by="name" collection="$ctrl.customers" result="$ctrl.paginator"></mv-paginator>
```

3. Utilizar o objeto especificado no parâmetro **result**.
```html
<div ng-repeat="customer in $ctrl.paginator">
  <p>{{::customer.name}}</p>
</div>
```
