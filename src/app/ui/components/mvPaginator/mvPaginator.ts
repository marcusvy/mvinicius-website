class MvPaginatorController {
  public limit: number;
  public current: number;
  public total: number;
  public orderBy: string;
  public result: any;
  public collection: any;
  private $filter: ng.IFilterService;

  /** @ngInject */
  constructor($filter: ng.IFilterService) {
    this.$filter = $filter;
    this.collection = [];

    this.$onInit = this.$onInit.bind(this);
    this.filter = this.filter.bind(this);
    this.next = this.next.bind(this);
    this.prev = this.prev.bind(this);
    this.isNextDisabled = this.isNextDisabled.bind(this);
    this.isPrevDisabled = this.isPrevDisabled.bind(this);
  }

  $onInit() {
    if (this.limit === undefined) {
      this.limit = 3;
    }
    if (this.current === undefined) {
      this.current = 0;
    }
    this.total = Math.ceil(this.collection.length / this.limit) - 1;
    console.log(this.collection);
    this.filter();
  }

  filter() {
    console.log("filter", this.collection);
    let offset = this.current * this.limit;
    let filterMvPaginate: any = this.$filter('mvPaginate');
    this.result = filterMvPaginate(this.collection, offset);
    this.result = this.$filter('limitTo')(this.result, this.limit);
    this.result = this.$filter('orderBy')(this.result, this.orderBy, false);
  }

  next() {
    if (this.current < this.total) {
      this.current++;
    }
    this.filter();
  }

  prev() {
    if (this.current > 0) {
      this.current--;
    }
    this.filter();
  }

  isNextDisabled() {
    return (this.current === this.total);
  }

  isPrevDisabled() {
    return (this.current === 0);
  }

}

export const mvPaginator = {
  templateUrl: 'app/ui/components/mvPaginator/mvPaginator.html',
  controller: MvPaginatorController,
  transclude: true,
  bindings: {
    limit: '<',
    orderby: '<',
    collection: '=',
    result: '='
  }
};
