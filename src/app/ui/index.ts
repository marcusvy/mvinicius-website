/// <reference path="../../../typings/index.d.ts" />
import * as angular from 'angular';

import {mvGrid} from './components/mvGrid/mvGrid';
import {mvGridItem} from './components/mvGridItem/mvGridItem';
import {mvLogo} from './components/mvLogo/mvLogo';
import {mvMoment} from './components/mvMoment/mvMoment';
import {mvPaginator} from './components/mvPaginator/mvPaginator';
import {mvPaginate} from './components/mvPaginator/mvPaginate';
import {mvSection} from './components/mvSection/mvSection';
import {mvSectionSlideshow} from './components/mvSectionSlideshow/mvSectionSlideshow';
import {mvServiceIcon} from './components/mvServiceIcon/mvServiceIcon';
import './index.scss';

export const mvUi: string = 'mvUi';

angular
  .module(mvUi, [])
  .component('mvGrid', mvGrid)
  .component('mvGridItem', mvGridItem)
  .component('mvLogo', mvLogo)
  .filter('mvMoment', mvMoment)
  .component('mvPaginator', mvPaginator)
  .filter('mvPaginate', mvPaginate)
  .component('mvSection', <any>mvSection)
  .component('mvSectionSlideshow', mvSectionSlideshow)
  .component('mvServiceIcon', mvServiceIcon);
