/**
 * Typography
 *
 *
 * @project mvComponents
 * @version 1.0
 * @package Core
 * @subpackage Typography
 * @author Marcus Vinícius da Rocha Gouveia Cardoso
 * @copyright 2016 by author
 * @licence MIT
 *
 * @section Configuration
 */

//## Font, line-height, and color for body text, headings, and more.
$font__family--sans-serif:  "Roboto",Helvetica, Arial, sans-serif !default;
$font__family--serif:       Georgia, "Times New Roman", Times, serif !default;
$font__family--monospace:   Menlo, Monaco, Consolas, "Courier New", monospace !default;
$font__family--base:        $font__family--sans-serif !default;

$font__size--base:          16px !default;
$font__size--large:         ceil(($font__size--base * 1.25)) !default; // ~18px
$font__size--small:         ceil(($font__size--base * 0.85)) !default; // ~12px

$font__size--h1:            floor(($font__size--base * 2.6)) !default; // ~36px
$font__size--h2:            floor(($font__size--base * 2.15)) !default; // ~30px
$font__size--h3:            ceil(($font__size--base * 1.7)) !default; // ~24px
$font__size--h4:            ceil(($font__size--base * 1.25)) !default; // ~18px
$font__size--h5:            $font__size--base !default;
$font__size--h6:            ceil(($font__size--base * 0.85)) !default; // ~12px

$font__weight--thin:        100;
$font__weight--light:       300;
$font__weight--normal:      400;
$font__weight--medium:      500;
$font__weight--bold:        700;
$font__weight--ultrabold:   900;

$font__weight--base: $font__weight--normal;

//** Unit-less `line-height` for use in components like buttons.
$line-height-base:        1.5 !default; // 24/16
//** Computed "line-height" (`font-size` * `line-height`) for use with `margin`, `padding`, etc.
$line-height-computed:    floor(($font__size--base * $line-height-base)) !default; // ~20px

//** By default, this inherits from the `<body>`.
$headings__font-family:    inherit !default;
$headings__font-weight:    300 !default;
$headings__line-height:    1.1 !default;
$headings__color:          inherit !default;

$blockquote__font-size: 4rem;
$blockquote__small-color:      $mv-color__light;
//** Blockquote font size
$blockquote__font-size:        ($font__size--base * 1.25);
//** Blockquote border color
$blockquote__border-color:     $mv-color__light;

$code__color: $mv-color__default;
$code__background-color: $mv-color__gray--100;
$code__border-raius: $mv-border-radius;

$kbd__color: $mv-color__default;
$kbd__background-color: $mv-color__gray--100;
$kbd__boder-radius: $mv-border-radius;

$pre__color: $mv-color__gray--500;
$pre__background-color: $mv-color__gray--100;
$pre__border-color: $mv-color__gray--100;
$pre__height--max : 2em;
$pre__border-radius: $mv-border-radius;

/*
 * @section Mixin
 */

@mixin clearfix() {
  &:before,
  &:after {
    content: " "; // 1
    display: table; // 2
  }
  &:after {
    clear: both;
  }
}

@mixin text-overflow() {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

/*
 * @section Headings
 */
h1, h2, h3, h4, h5, h6,
.h1, .h2, .h3, .h4, .h5, .h6 {
  font-family: $headings__font-family;
  font-weight: $headings__font-weight;
  line-height: $headings__line-height;
  color: $mv-color__default;

  small,
  .small {
    font-weight: normal;
    line-height: 1;
    color: $mv-color__gray--500;
  }
}

h1, .h1,
h2, .h2,
h3, .h3 {
  margin-top: $line-height-computed;
  margin-bottom: ($line-height-computed / 2);

  small,
  .small {
    font-size: 50%;
  }
}
h4, .h4,
h5, .h5,
h6, .h6 {
  margin-top: ($line-height-computed / 2);
  margin-bottom: ($line-height-computed / 2);

  small,
  .small {
    font-size: 55%;
  }
}

h1, .h1 { font-size: $font__size--h1; }
h2, .h2 { font-size: $font__size--h2; }
h3, .h3 { font-size: $font__size--h3; }
h4, .h4 { font-size: $font__size--h4; }
h5, .h5 { font-size: $font__size--h5; }
h6, .h6 { font-size: $font__size--h6; }


/*
 * @section Body text
 */
p {
  margin: 0 0 ($line-height-computed / 2);
}

.lead {
  margin-bottom: $line-height-computed;
  font-size: floor(($font__size--base * 1.15));
  font-weight: 300;
  line-height: 1.4;

  @media (min-width: $mv-device__sm) {
    font-size: ($font__size--base * 1.5);
  }
}


/*
 * @section Emphasis & misc
 */
// Ex: (12px small font / 14px base font) * 100% = about 85%
small,
.small {
  font-size: floor((100% * $font__size--small / $font__size--base));
}

// Undo browser default styling
cite {
  font-style: normal;
}

mark,
.mark {
  background-color: $mv-color__warning;
  padding: .2em;
}

// Alignment
.text-left           { text-align: left; }
.text-right          { text-align: right; }
.text-center         { text-align: center; }
.text-justify        { text-align: justify; }
.text-nowrap         { white-space: nowrap; }

// Transformation
.text-lowercase      { text-transform: lowercase; }
.text-uppercase      { text-transform: uppercase; }
.text-capitalize     { text-transform: capitalize; }

// Contextual colors
.text-muted {
  color: $mv-color__gray--300;
}

/*
 * @section Lists
 */
ul,
ol {
  margin-top: 0;
  margin-bottom: ($line-height-computed / 2);
  ul,
  ol {
    margin-bottom: 0;
  }
}

.no-list {
  padding-left: 0;
  list-style: none;
}

.list-inline {
  @extend .no-list;
  margin-left: -5px;

  > li {
    display: inline-block;
    padding-left: 5px;
    padding-right: 5px;
  }
}

dl {
  margin-top: 0; // Remove browser default
  margin-bottom: $line-height-computed;
}
dt,
dd {
  line-height: $line-height-base;
}
dt {
  font-weight: bold;
}
dd {
  margin-left: 0; // Undo browser default
}

.dl-horizontal {
  dd {

    @include clearfix(); // Clear the floated `dt` if an empty `dd` is present
  }

  @media (min-width: $mv-device__sm) {
    dt {
      float: left;
      width: ($mv-size__height - 20);
      clear: left;
      text-align: right;
      @include text-overflow();
    }
    dd {
      margin-left: $mv-size__height;
    }
  }
}

/*
 * @section Misc
 */
// Abbreviations and acronyms
abbr[title],
  // Add data-* attribute to help out our tooltip plugin, per https://github.com/twbs/bootstrap/issues/5257
abbr[data-original-title] {
  cursor: help;
  border-bottom: 1px solid $mv-color-_gray--500;
}
.initialism {
  font-size: 90%;
  text-transform: uppercase;
}

// Blockquotes
blockquote {
  padding: ($line-height-computed / 2) $line-height-computed;
  margin: 0 0 $line-height-computed;
  font-size: $blockquote__font-size;
  border-left: 5px solid $mv-color-_gray--500;

  p,
  ul,
  ol {
    &:last-child {
      margin-bottom: 0;
    }
  }

  // Note: Deprecated small and .small as of v3.1.0
  // Context: https://github.com/twbs/bootstrap/issues/11660
  footer,
  small,
  .small {
    display: block;
    font-size: 80%; // back to default font-size
    line-height: $line-height-base;
    color: $blockquote__small-color;

    &:before {
      content: '\2014 \00A0'; // em dash, nbsp
    }
  }
}

// Opposite alignment of blockquote
//
// Heads up: `blockquote.pull-right` has been deprecated as of v3.1.0.
.blockquote-reverse,
blockquote.pull-right {
  padding-right: 15px;
  padding-left: 0;
  border-right: 5px solid $blockquote__border-color;
  border-left: 0;
  text-align: right;

  // Account for citation
  footer,
  small,
  .small {
    &:before { content: ''; }
    &:after {
      content: '\00A0 \2014'; // nbsp, em dash
    }
  }
}

// Quotes
blockquote:before,
blockquote:after {
  content: "";
}

// Addresses
address {
  margin-bottom: $line-height-computed;
  font-style: normal;
  line-height: $line-height-base;
}

/*
 * @section Code (inline and block)
 */
code,
kbd,
pre,
samp {
  font-family: $font__family--monospace;
}

// Inline code
code {
  padding: 2px 4px;
  font-size: 90%;
  color: $code__color;
  background-color: $code__background-color;
  border-radius: $code__border-raius;
}

// User input typically entered via keyboard
kbd {
  padding: .2rem .5rem;
  font-size: 90%;
  color: $kbd__color;
  background-color: $kbd__background-color;
  border-radius: $kbd__boder-radius;
  box-shadow: inset 0 -1px 0 rgba(0,0,0,.25);

  kbd {
    padding: 0;
    font-size: 100%;
    box-shadow: none;
  }
}

//// Blocks of code
pre {
  display: block;
  padding: (($line-height-computed - 1) / 2);
  margin: 0 0 ($line-height-computed / 2);
  font-size: ($font__size--base - 1); // 14px to 13px
  line-height: $line-height-base;
  word-break: break-all;
  word-wrap: break-word;
  color: $pre__color;
  background-color: $pre__background-color;
  border: 1px solid $pre__border-color;
  border-radius: $pre__border-radius;

  // Account for some code outputs that place code tags in pre tags
  code {
    padding: 0;
    font-size: inherit;
    color: inherit;
    white-space: pre-wrap;
    background-color: transparent;
    border-radius: 0;
  }
}

// Enable scrollable blocks of code
.pre-scrollable {
  max-height: $pre__height--max;
  overflow-y: scroll;
}
